---
pagetitle: "1-Introducción al curso"
title: "Taller de R Aplicado a la Investigación en Ciencias Económicas y Empresariales"
subtitle: "**Semana 1:** Introducción al curso"
author: 
      name: "**Eduard Fernando Martínez-González**"
      affiliation: Facultad de Ciencias Económicas y Empresariales, Universidad del Magdalena  #[`r fontawesome::fa('globe')`]()
# date: Lecture 10  #"`r format(Sys.time(), '%d %B %Y')`"
output: 
  html_document:
    theme: flatly
    highlight: haddock
    # code_folding: show
    toc: yes
    toc_depth: 4
    toc_float: yes
    keep_md: false
    keep_tex: false ## Change to true if want keep intermediate .tex file
    ## For multi-col environments
  pdf_document:
    latex_engine: xelatex
    toc: true
    dev: cairo_pdf
    # fig_width: 7 ## Optional: Set default PDF figure width
    # fig_height: 6 ## Optional: Set default PDF figure height
    includes:
      in_header: tex/preamble.tex ## For multi-col environments
    pandoc_args:
        --template=tex/mytemplate.tex ## For affiliation field. See: https://bit.ly/2T191uZ
always_allow_html: true
urlcolor: blue
mainfont: cochineal
sansfont: Fira Sans
monofont: Fira Code ## Although, see: https://tex.stackexchange.com/q/294362
## Automatically knit to both formats
---

```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome,kableExtra)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

<!--==================-->
<!--==================-->
## **[1.] Acerca del curso**

<!--------------------->
### **1.1 Motivación**

* Bajamos 5.206 ofertas de empleo para economistas en Colombia posteadas en Computrabajo (vigentes en este momento).

![](pics/word_cloud.jpeg){width=50%}

* Entre 1.400 y 1.600 requieren que el candidato tenga manejo de bases de datos.

![](pics/freq_jobs.jpeg){width=50%}

### **1.2 Horario y profesores**

#### **1.2.1 Horario de clases:**

`r fontawesome::fa('book-open')` [Syllabus](https://eduard-martinez.github.io/teaching/r_uniandes/2022-02/syllabus.pdf)

`r fa('globe')` Página web del curso [aquí](https://github.com/taller-r-202202) 

`r fa('clock')` Sábado 08:00 – 11:00, Sala de Microsoft Teams

#### **1.2.2 Acerca de mí:**

`r fa('graduation-cap')` Estudiante doctorado en economía de la Universidad de los Andes

`r fa('globe')` [https://eduard-martinez.github.io](https://eduard-martinez.github.io)

`r fa('envelope')` ef.martinezg@uniandes.edu.co 

`r fa('github')` <a href="https://github.com/eduard-martinez" style="color:black;"> eduard-martinez </a> 

`r fa('twitter')` <a href="https://twitter.com/emartigo" style="color:black;"> @emartigo </a> 
  
#### **1.2.3 Horario de atención:**

- Selena Arias, `r fa('envelope')` selenaariasyv@unimagdalena.edu.co

- Alejandro Cardiles, `r fa('envelope')` alejandrocardilesjh@unimagdalena.edu.co

<!--------------------->
### **1.3 Objetivo**

Este curso busca promover el uso de lenguajes de programación de código abierto (open source), familiarizando al estudiante con el entorno de trabajo de R e introduciendo a los estudiantes en algunos métodos computacionales aplicados en la investigación económica.

Al finalizar este curso, los estudiantes podrán manipular grandes conjuntos de datos para emplear técnicas de análisis de datos que les permitan responder preguntas de investigación aplicadas en las ciencias económicas y empresariales. 

<!--------------------->
### **1.4 Organización del curso**

El curso está dividido en 6 módulos:

* **Módulo 1:** Fundamentos de programación.
* **Módulo 2:** Replicabilidad de proyectos.
* **Módulo 3:** Pre-procesamiento de conjuntos de datos.
* **Módulo 4:** Visualización de información.
* **Módulo 5:** Introducción a la estadística inferencial en R.
* **Módulo 6:** Otras aplicaciones.
* **Módulo 7:** Presentación de resultados.

<!--------------------->
### **1.5 Metodología**

Todas las clases tienen un componente teórico y un componente practico. Inicialmente se desarrollaran ejercicios con conjuntos de datos pequeños que permitan asimilar el concepto de cada función y posteriormente se desarrollara una aplicación con un conjunto de datos de una fuente oficial (ICFES, DANE y/o Datos Abiertos).

<!--------------------->
### **1.6 Evaluación**

Para evaluar este curso, cada estudiante deberá seleccionar un conjunto de datos del Departamento Administrativo Nacional de Estadísticas de Colombia y presentará antes los demás estudiantes del curso un análisis detallado del conjunto de datos. Próximamente les estaré compartiendo por correo una descripción detallada de este trabajo.

<!--------------------->
### **1.7 Al finalizar este curso:**

**No les voy a mentir...**

![](pics/curva_aprendizaje.jpg){width=40%}

**Pero...**

![](pics/awesome.jpg){width=70%}

<!--------------------->
<!--------------------->
## **2. ¿Usar R?**

R es un entorno de programación de código abierto (libre) que fue desarrollado en 1993 por Robert Gentleman y Ross Ihaka del Departamento de Estadística de la Universidad de Auckland.

#### **2.1 Lenguaje orientado a objetos (OOP)**

-   Todo es un objeto.

    -   Puede trabajar con más de una base de datos (objeto) al mismo tiempo (los usuarios de Stata no necesitarán más *keep*, *preserve* o *restore*).

-   Todo tiene un nombre

#### **2.2 Librerías y funciones**

-   Las funciones permiten crear, editar, transformar o eliminar objetos.

-   Las librerías siempre se cargan al iniciar la sesión de R.

-   Puedes usar funciones creadas por otros usuarios.

#### **2.3 Popularidad**

Una aproximación a la popularidad de un software es la demanda del lenguaje de programación en el mercado laboral:

```{r echo=FALSE, message=FALSE, warning=FALSE, out.width="60%"}
pop_df =  data.frame( lang = c("SQL", "Python", "R", "SAS", "Matlab", "SPSS", "Stata"),
                      n_jobs = c(107130, 66976, 48772, 25644, 11464, 3717, 1624),
                      free = c(T, T, T, F, F, F, F))

### Plot it
pop_df %>%  mutate(lang = lang %>% factor(ordered = T)) %>%
            ggplot(aes(x = lang, y = n_jobs, fill = free)) +
            geom_col() + geom_hline(yintercept = 0) +
            aes(x = reorder(lang, -n_jobs), fill = reorder(free, -free)) +
            xlab("Lenguaje estadístico") + scale_y_continuous(label = scales::comma) +
            ylab("Número de trabajos") +
            labs( title = "Número de ofertas de trabajo posteadas en Indeed.com, 2019/01/06") +
            scale_fill_manual( "Código abierto?", labels = c("Sí", "No"), values = c("cadetblue1", "chocolate1")) + theme_minimal() + theme(legend.position = "bottom") 

##nrow(available.packages())
```

Fuente: [https://github.com/uo-ec607](https://github.com/uo-ec607)

#### **2.4 Ideal para la ciencia de datos**

-   R y Python son 2 de los lenguajes de programación más populares en la ciencia de datos

    -   Ver: [The Popularity of Data Science Software](http://r4stats.com/articles/popularity/)

-   Cada vez es más frecuente su uso en diferentes industrias (académica, manufacturera,...)

    -   Ver: [The Impressive Growth of R](https://stackoverflow.blog/2017/10/10/impressive-growth-r/)

#### **2.5 Open-source (free!)**

-   Se puede descargar, instalar y utilizar sin coste alguno.

-   R cuenta con `r ` librerías disponibles en el CRAN. (`r format(Sys.time(), "%Y-%m-%d")`)

<!--------------------->
<!--------------------->
## **3. Configuración inicial**

Antes de iniciar, asegúrese de tener instalado `R` y `Rstudio`.

#### **1.1 Descargar [R](https://cran.r-project.org/)**

R es un software de acceso libre usado para el análisis estadístico y creación de gráficos. Funciona en una amplia variedad de plataformas UNIX, Windows y MacOS.

#### **1.2 Descargar [RStudio](https://www.rstudio.com/products/rstudio/download/preview/)**

RStudio es un entorno de desarrollo integrado (IDE) para el lenguaje de programación R. Esta IDE brinda una interfaz más amigable con el usuario facilitando el aprendizaje.

#### **1.3 Instalar R y RStudio**

Puede ir a este [enlace](https://lectures-r.gitlab.io/uniandes-202202/initial-setup) y seguir las instrucciones de instalación para el sistema operativo de su equipo.

<!--------------------->
<!--------------------->
## **4. Interfaz de R y Rstudio**

#### **4.1 Consola de R**

![](pics/interfaz_r.png){width="529"}

#### **4.2. IDE: RStudio**

![](pics/interfaz_rstudio.png){width="541"}

##### **4.2.1 Consola**

La consola permite ejecutar una línea de código y visualizar el resultado de ejecutar un código. Sin embargo, cuando se antepone un ***\#*** a una línea de código, **R** pinta la línea de código sobre la consola pero no la ejecuta.

![](pics/consola.png){width="538"}

Después de ejecutar una línea de código en R, la consola puede retornar un **decoding messages**. Estos pueden ser:

[Warning]() o [message](): sugiere que hay detalles de la función que debemos tener en cuenta (no se detiene la ejecución de la función).

[Error](): se genera cuando ocurrió un error importante causando que la función no continue ejecutándose.

![](pics/decoding.png){width="537"}

##### **4.2.2 Entorno de trabajo**

El entorno de trabajo de R almacena temporalmente los objetos que se asignan durante una sesión. Al momento de cerrar la sesión Rstudio nos preguntará si queremos almacenar en un archivo `.Rdata` los objetos que se encuentran en la memoria activa de R.

![](pics/environment.gif){width="524"}

##### **4.2.3 Editor de sintaxis**

El editor de sintaxis permite escribir las instrucciones que se van a ejecutar en R. Para ejecutar una línea de código del editor de sintaxis se debe sombrear toda la línea de código y se presionan las teclas [Control]() + [Enter]() o se hace clik sobre el boton [run]().

<img src="pics/editor_sintaxis.png" width="514" height="388"/>




